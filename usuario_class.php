classe  Usuário {
        público  $nome ;
        privado  $cpf ;
        privado  $endereco ;
    

        function  __construct ( $nome_externo ,  $cpf ,  $endereco ){
            $this -> nome  =  $nome_externo ;
            $this -> cpf  =  $cpf ;
            $this -> endereco  =  $endereco ;
        }


        função  getCpf (){
            return  $this -> cpf ;
        }

        função  getEndereco (){
            return  $this -> endereco ;
        }

    }

?>
